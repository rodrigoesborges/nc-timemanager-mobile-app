import 'dart:math' as math;

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../../helper/accumulated_duration.dart';
import '../../helper/duration_format.dart';
import '../../helper/i18n_helper.dart';
import '../../model/client.dart';
import '../../model/project.dart';
import '../../model/task.dart';
import '../../model/time.dart';
import '../../provider/settings_provider.dart';
import '../../provider/timemanager_provider.dart';
import '../time_entries_modal.dart';

class Chart extends StatefulWidget {
  @override
  _ChartState createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  Client? _selectedClient;
  Project? _selectedProject;
  Task? _selectedTask;
  PeriodMode _periodMode = PeriodMode.Month;
  AccumulatedDuration? _periodFilter;

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);

    List<Task>? tasks;
    List<Project>? projects;
    List<Client>? clients;
    if (_selectedProject != null) {
      tasks = _selectedProject!.tasks.toList();
      tasks.sort((one, other) =>
          one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    } else if (_selectedClient != null) {
      projects = _selectedClient!.projects.toList();
      projects.sort((one, other) =>
          one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    } else {
      clients = tm.getAll<Client>().toList();
      clients.sort((one, other) =>
          one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    }

    List<Time> times;
    if (_selectedTask != null) {
      times = _selectedTask!.times.toList();
    } else if (_selectedProject != null) {
      times = tasks!.expand((task) => task.times).toList();
    } else if (_selectedClient != null) {
      times = projects!.expand((project) => project.times).toList();
    } else {
      times = clients!.expand((client) => client.times).toList();
    }
    times.sort((a, b) => b.end.compareTo(a.end));

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            'dashboard.chart.charts'.tl(context),
            textScaleFactor: 1.4,
          ),
          SizedBox(height: 10),
          if (_selectedClient != null)
            TextButton.icon(
              onPressed: () {
                _periodFilter = null;
                _selectedTask = null;
                if (_selectedProject != null) {
                  setState(() {
                    _selectedProject = null;
                  });
                } else {
                  setState(() {
                    _periodFilter = null;
                    _selectedClient = null;
                  });
                }
              },
              icon: Icon(Icons.arrow_back),
              label: Text(
                _selectedClient!.name +
                    (_selectedProject != null
                        ? ' - ' + _selectedProject!.name
                        : ''),
              ),
            ),
          Text(
            (_selectedProject != null
                    ? 'general.tasks'
                    : _selectedClient != null
                        ? 'general.projects'
                        : 'general.clients')
                .tl(context),
            textScaleFactor: 1.3,
          ),
          // TODO: Add Search filter
          tasks != null
              ? _taskChart(tasks)
              : projects != null
                  ? _projectChart(projects)
                  : _clientChart(clients!),
          SizedBox(height: 25),
          TextButton.icon(
            icon: Icon(
              Icons.insert_chart_outlined,
              size: 30,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              setState(() {
                _periodFilter = null;
                _periodMode = _periodMode == PeriodMode.Day
                    ? PeriodMode.Week
                    : _periodMode == PeriodMode.Week
                        ? PeriodMode.Month
                        : _periodMode == PeriodMode.Month
                            ? PeriodMode.Year
                            : PeriodMode.Day;
              });
            },
            label: Text(
              'dashboard.chart.time_per'.tl(context) +
                  ' ' +
                  (_periodMode == PeriodMode.Day
                      ? 'dashboard.chart.day'.tl(context)
                      : _periodMode == PeriodMode.Week
                          ? 'dashboard.chart.week_of_year'.tl(context)
                          : _periodMode == PeriodMode.Month
                              ? 'dashboard.chart.month'.tl(context)
                              : 'dashboard.chart.year'.tl(context)),
              textScaleFactor: 1.3,
            ),
          ),
          _timesChart(times),
        ],
      ),
    );
  }

  Widget _clientChart(List<Client> clients) {
    final c = Theme.of(context).primaryColor;
    final color = charts.Color(r: c.red, g: c.green, b: c.blue);
    return _defaultChart(
      charts.Series<Client, String>(
        id: 'Clients',
        domainFn: (client, _) =>
            client.name.substring(0, math.min(client.name.length, 20)),
        measureFn: (client, _) {
          final d = _periodFilter == null
              ? client.duration
              : client.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return (d.inMinutes % 60) / 60 + d.inHours;
        },
        labelAccessorFn: (client, _) {
          final d = _periodFilter == null
              ? client.duration
              : client.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return d.toFormattedString(
              joiner: '\n',
              showDays: Provider.of<SettingsProvider>(context, listen: false)
                  .showDays);
        },
        colorFn: (client, _) => color,
        data: clients,
      ),
      (model) {
        if (model.hasDatumSelection) {
          setState(() {
            _selectedClient =
                model.selectedSeries[0].data[model.selectedDatum[0].index!];
          });
        }
      },
      clients.length,
    );
  }

  Widget _projectChart(List<Project> projects) {
    final c = Theme.of(context).primaryColor;
    final color = charts.Color(r: c.red, g: c.green, b: c.blue);
    return _defaultChart(
      charts.Series<Project, String>(
        id: 'Project',
        domainFn: (project, _) =>
            project.name.substring(0, math.min(project.name.length, 20)),
        measureFn: (project, _) {
          final d = _periodFilter == null
              ? project.duration
              : project.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return (d.inMinutes % 60) / 60 + d.inHours;
        },
        labelAccessorFn: (project, _) {
          final d = _periodFilter == null
              ? project.duration
              : project.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return d.toFormattedString(
              joiner: '\n',
              showDays: Provider.of<SettingsProvider>(context, listen: false)
                  .showDays);
        },
        colorFn: (project, _) => color,
        data: projects,
      ),
      (charts.SelectionModel model) {
        if (model.hasDatumSelection) {
          setState(() {
            _selectedProject =
                model.selectedSeries[0].data[model.selectedDatum[0].index!];
          });
        }
      },
      projects.length,
    );
  }

  Widget _taskChart(List<Task> tasks) {
    final c = Theme.of(context).primaryColor;
    final c2 = Theme.of(context).accentColor;
    final color = charts.Color(r: c.red, g: c.green, b: c.blue);
    final color2 = charts.Color(r: c2.red, g: c2.green, b: c2.blue);
    return _defaultChart(
      charts.Series<Task, String>(
        id: 'Tasks',
        domainFn: (task, _) =>
            task.name.substring(0, math.min(task.name.length, 20)),
        measureFn: (task, _) {
          final d = _periodFilter == null
              ? task.duration
              : task.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return (d.inMinutes % 60) / 60 + d.inHours;
        },
        labelAccessorFn: (task, _) {
          final d = _periodFilter == null
              ? task.duration
              : task.getDurationInPeriod(
                  _periodFilter!.mode, _periodFilter!.key);
          return d.toFormattedString(
              joiner: '\n',
              showDays: Provider.of<SettingsProvider>(context, listen: false)
                  .showDays);
        },
        colorFn: (task, _) =>
            _selectedTask == null || _selectedTask != task ? color : color2,
        data: tasks,
      ),
      (charts.SelectionModel model) async {
        if (model.hasDatumSelection) {
          final newSelectedTask =
              model.selectedSeries[0].data[model.selectedDatum[0].index!];
          if (_selectedTask != null && _selectedTask == newSelectedTask) {
            await showModalBottomSheet(
              elevation: 4,
              context: context,
              builder: (ctx) {
                return GestureDetector(
                  onTap: () {},
                  child: TimeEntriesModal(newSelectedTask.uuid),
                  behavior: HitTestBehavior.opaque,
                );
              },
            );
          } else {
            setState(() {
              _selectedTask = newSelectedTask;
            });
          }
        }
      },
      tasks.length,
    );
  }

  Widget _timesChart(List<Time> times) {
    final c = Theme.of(context).primaryColor;
    final c2 = Theme.of(context).accentColor;
    final color = charts.Color(r: c.red, g: c.green, b: c.blue);
    final color2 = charts.Color(r: c2.red, g: c2.green, b: c2.blue);
    Map<String, AccumulatedDuration> durations = {};
    times.forEach((time) {
      final _ad =
          AccumulatedDuration(context, _periodMode, time.duration, time);
      durations.update(
        _ad.key,
        (prev) => _ad + prev,
        ifAbsent: () => _ad,
      );
    });
    durations.removeWhere((key, value) => value.duration.inSeconds == 0);
    var values = durations.values.toList();
    values.sort();
    // Max 100 elements
    if (values.length > 100) {
      values = values.sublist(0, 100);
    }
    return _defaultChart(
      charts.Series<AccumulatedDuration, String>(
        id: 'Times',
        domainFn: (durations, _) => durations.label,
        measureFn: (durations, _) =>
            (durations.duration.inMinutes % 60) / 60 +
            durations.duration.inHours,
        labelAccessorFn: (durations, _) => durations.duration.toFormattedString(
            joiner: '\n',
            showDays:
                Provider.of<SettingsProvider>(context, listen: false).showDays),
        colorFn: (durations, _) =>
        _periodFilter == null || _periodFilter!.key != durations.key
                ? color
                : color2,
        data: values,
      ),
      (charts.SelectionModel model) {
        final newSelectedPeriod =
            model.selectedSeries[0].data[model.selectedDatum[0].index!];
        setState(() {
          _periodFilter = _periodFilter != null &&
                  _periodFilter!.key == newSelectedPeriod.key
              ? null
              : newSelectedPeriod;
        });
      },
      values.length,
    );
  }

  Widget _defaultChart(
    charts.Series<dynamic, String> series,
    Function(charts.SelectionModel) changedListener,
    itemCount,
  ) {
    final b = Theme.of(context).brightness.index;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: 300,
          maxWidth: math.max(itemCount * 75.0, 300),
          minWidth: math.max(itemCount * 75.0, 300),
        ),
        child: charts.BarChart(
          [series],
          selectionModels: [
            charts.SelectionModelConfig(
              changedListener: changedListener,
            )
          ],
          domainAxis: charts.OrdinalAxisSpec(
            renderSpec: charts.SmallTickRendererSpec(
              labelRotation: 50,
              labelStyle: charts.TextStyleSpec(
                fontSize: 15,
                color: b == 1
                    ? charts.MaterialPalette.black
                    : charts.MaterialPalette.white,
              ),
              lineStyle: charts.LineStyleSpec(
                color: b == 1
                    ? charts.MaterialPalette.black
                    : charts.MaterialPalette.white,
              ),
            ),
          ),
          primaryMeasureAxis: charts.NumericAxisSpec(
            renderSpec: charts.SmallTickRendererSpec(
              labelStyle: charts.TextStyleSpec(
                fontSize: 15,
                color: b == 1
                    ? charts.MaterialPalette.black
                    : charts.MaterialPalette.white,
              ),
              lineStyle: charts.LineStyleSpec(
                color: b == 1
                    ? charts.MaterialPalette.black
                    : charts.MaterialPalette.white,
              ),
            ),
          ),
          animate: true,
          vertical: true,
          behaviors: [charts.PanAndZoomBehavior()],
          barRendererDecorator: charts.BarLabelDecorator<String>(),
        ),
      ),
    );
  }
}
