import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../helper/i18n_helper.dart';
import '../../helper/share_times.dart';
import '../../model/time.dart';
import '../../provider/timemanager_provider.dart';

class ShareButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      icon: Icon(
        Icons.share,
        size: 25,
        color: Theme.of(context).accentColor,
      ),
      label: Text('general.export_share_times'.tl(context)),
      onPressed: () {
        final tm = Provider.of<TimemanagerProvider>(context, listen: false);
        final times = tm.getAll<Time>().toList();
        ShareTimes.share(times, context); // TODO: filter?
      },
    );
  }
}
