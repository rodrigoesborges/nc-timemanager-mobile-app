import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../helper/i18n_helper.dart';
import '../../model/abstract_timemanager_object.dart';
import '../../provider/timemanager_provider.dart';

class TimemanagerObjectGridItem extends StatelessWidget {
  final AbstractTimemanagerObject timeMangerObject;
  final void Function() onTap;
  final void Function()? onLongPress;
  final Future<void> Function() onShare;
  final Icon leadingIcon;
  final String titleText;
  final Widget subtitle;
  final Widget? trailing;

  const TimemanagerObjectGridItem({
    required this.timeMangerObject,
    required this.onTap,
    this.onLongPress,
    required this.leadingIcon,
    required this.titleText,
    required this.subtitle,
    this.trailing,
    required this.onShare,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).accentColor,
      onTap: onTap,
      onLongPress: onLongPress,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.elliptical(5, 10),
            top: Radius.elliptical(5, 10),
          ),
        ),
        clipBehavior: Clip.hardEdge,
        child: Dismissible(
          key: ValueKey(timeMangerObject.uuid),
          direction: DismissDirection.horizontal,
          background: Container(
            padding: EdgeInsets.only(left: 20),
            alignment: Alignment.centerLeft,
            color: Colors.green,
            child: Icon(
              Icons.share,
              color: Colors.white,
              size: 40,
            ),
          ),
          secondaryBackground: Container(
            padding: EdgeInsets.only(right: 20),
            alignment: Alignment.centerRight,
            color: Theme.of(context).errorColor,
            child: Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
          ),
          onDismissed: (direction) {
            if (direction == DismissDirection.endToStart) {
              final tm =
                  Provider.of<TimemanagerProvider>(context, listen: false);
              tm.delete(timeMangerObject);
              tm.sync();
            }
          },
          confirmDismiss: (direction) async {
            if (direction == DismissDirection.startToEnd) {
              await onShare();
              return false;
            } else {
              return showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                  title: Text('dialog.are_you_sure'.tl(context)),
                  content: Text('dialog.delete_item'.tl(context)),
                  actions: [
                    TextButton(
                      child: Text(
                          MaterialLocalizations.of(context).cancelButtonLabel),
                      onPressed: () => Navigator.of(ctx).pop(false),
                    ),
                    TextButton(
                      child:
                          Text(MaterialLocalizations.of(context).okButtonLabel),
                      onPressed: () => Navigator.of(ctx).pop(true),
                    ),
                  ],
                ),
              );
            }
          },
          child: Container(
            height: double.infinity,
            child: GridTileBar(
              backgroundColor: Theme.of(context).primaryColor,
              leading: leadingIcon,
              title: Text(
                titleText,
                textScaleFactor: 1.3,
                maxLines: 2,
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  subtitle,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'general.created'.tl(context) +
                            ':\n' +
                            DateFormat.yMMMd(Localizations.localeOf(context)
                                    .languageCode)
                                .format(timeMangerObject.created!),
                        textScaleFactor: 0.8,
                      ),
                      Divider(),
                      Text(
                        'general.updated'.tl(context) +
                            ':\n' +
                            DateFormat.yMMMd(Localizations.localeOf(context)
                                    .languageCode)
                                .format(timeMangerObject.changed!),
                        textScaleFactor: 0.8,
                      ),
                    ],
                  ),
                ],
              ),
              trailing: trailing,
            ),
          ),
        ),
      ),
    );
  }
}
