import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './timemanager_object_grid_item.dart';
import '../../helper/date_time_helper.dart';
import '../../helper/duration_format.dart';
import '../../helper/i18n_helper.dart';
import '../../helper/share_times.dart';
import '../../model/client.dart';
import '../../provider/settings_provider.dart';
import '../../provider/timemanager_provider.dart';
import '../../screens/project_overview_screen.dart';

class ClientGridItem extends StatelessWidget {
  final String clientId;
  final Function() onLongPress;

  const ClientGridItem(this.clientId, this.onLongPress);

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    final tm = Provider.of<TimemanagerProvider>(context);
    final client = tm.get<Client>(clientId)!;
    return TimemanagerObjectGridItem(
      timeMangerObject: client,
      onTap: () => Navigator.of(context).pushNamed(
        ProjectOverviewScreen.routeName,
        arguments: client.uuid,
      ),
      onLongPress: onLongPress,
      leadingIcon: Icon(Icons.person, size: 30),
      titleText: client.name,
      subtitle: Container(
        height: 90,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('general.projects'.tl(context) +
                ': ' +
                client.projects.length.toString()),
            Text('general.today'.tl(context) +
                ': ' +
                client
                    .getDurationBetween(DateTime.now().getStartOfDay(),
                        DateTime.now().getEndOfDay())
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.this_week'.tl(context) +
                ': ' +
                client
                    .getDurationBetween(
                        DateTime.now()
                            .getStartOfWeek(startOfWeek: settings.startOfWeek),
                        DateTime.now()
                            .getEndOfWeek(startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.last_week'.tl(context) +
                ': ' +
                client
                    .getDurationBetween(
                        DateTime.now().getStartOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek),
                        DateTime.now().getEndOfWeek(
                            weekOffset: -1, startOfWeek: settings.startOfWeek))
                    .toFormattedString(showDays: settings.showDays)),
            Text('general.total'.tl(context) +
                ': ' +
                client.duration.toFormattedString(showDays: settings.showDays)),
          ],
        ),
      ),
      onShare: () => ShareTimes.share(client.times.toList(), context),
    );
  }
}
