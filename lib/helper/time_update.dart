import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../helper/duration_format.dart';
import '../helper/i18n_helper.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/task.dart';
import '../model/time.dart';
import '../provider/settings_provider.dart';
import '../provider/timemanager_provider.dart';

Future<void> updateTime(BuildContext context, Task task,
    [Time? time, copy = false]) async {
  final settings = Provider.of<SettingsProvider>(context, listen: false);
  String note = time == null ? '' : time.note;
  DateTime selectedDate = time == null ? DateTime.now() : time.start;
  DateTime selectedDateEnd =
      time == null ? DateTime.now().add(settings.defaultDuration) : time.end;
  if (selectedDateEnd.day == selectedDate.add(Duration(days: 1)).day) {
    selectedDateEnd = new DateTime(
        selectedDate.year, selectedDate.month, selectedDate.day, 23, 59, 59);
  }
  Duration selectedDuration =
      time == null ? settings.defaultDuration : time.duration;

  Future<void> _selectDate() async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2019, 1),
        lastDate: DateTime(DateTime.now().year + 1));
    if (picked != null && picked != selectedDate) selectedDate = picked;
  }

  Future<void> _selectDuration() async {
    final result = await showTimePicker(
      context: context,
      helpText: 'general.select_duration'.tl(context),
      initialTime: TimeOfDay(
          hour: selectedDuration.inHours,
          minute: selectedDuration.inMinutes % 60),
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
    );
    if (result != null) {
      selectedDuration = Duration(hours: result.hour, minutes: result.minute);
    }
  }

  Future<void> _selectTime(bool startTime) async {
    final result = await showTimePicker(
      context: context,
      helpText: 'general.select_time'.tl(context),
      initialTime: startTime
          ? TimeOfDay(hour: selectedDate.hour, minute: selectedDate.minute)
          : TimeOfDay(
              hour: selectedDateEnd.hour, minute: selectedDateEnd.minute),
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
    );
    if (result != null) {
      if (startTime) {
        selectedDate = new DateTime(
          selectedDate.year,
          selectedDate.month,
          selectedDate.day,
          result.hour,
          result.minute,
          0,
        );
        if (selectedDateEnd.isBefore(selectedDate)) {
          selectedDateEnd = new DateTime(selectedDate.year, selectedDate.month,
              selectedDate.day, selectedDate.hour, selectedDate.minute, 0);
        }
      } else {
        selectedDateEnd = new DateTime(
          selectedDate.year,
          selectedDate.month,
          selectedDate.day,
          result.hour,
          result.minute,
          0,
        );
        if (selectedDateEnd.isBefore(selectedDate)) {
          selectedDate = new DateTime(
              selectedDateEnd.year,
              selectedDateEnd.month,
              selectedDateEnd.day,
              selectedDateEnd.hour,
              selectedDateEnd.minute,
              0);
        }
      }
    }
  }

  final ok = await showDialog<bool>(
    context: context,
    builder: (context) => AlertDialog(
      contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      actionsPadding: EdgeInsets.all(5),
      buttonPadding: EdgeInsets.all(5),
      title: Text(time == null || copy
          ? 'time_modal.create_time'.tl(context)
          : 'time_modal.edit_time'.tl(context)),
      content: SingleChildScrollView(
        child: StatefulBuilder(builder: (context, setState2) {
          var selDate = selectedDate;
          var selDuration = selectedDuration;
          var selDateEnd = selectedDateEnd;
          return Column(
            children: [
              TextFormField(
                initialValue: note,
                decoration: InputDecoration(
                  labelText: 'general.note'.tl(context),
                ),
                onChanged: (value) => note = value,
              ),
              TextButton.icon(
                icon: Icon(Icons.date_range),
                onPressed: () async {
                  await _selectDate();
                  setState2(() => selDate = selectedDate);
                },
                label: Text(
                  DateFormat.yMMMEd(
                          Localizations.localeOf(context).languageCode)
                      .format(selDate),
                  textScaleFactor: 1.1,
                ),
              ),
              settings.recordingType == RecordingType.Duration
                  ? TextButton.icon(
                      icon: Icon(Icons.timelapse),
                      onPressed: () async {
                        await _selectDuration();
                        setState2(() => selDuration = selectedDuration);
                      },
                      label: Text(
                        selDuration.toFormattedString(
                            showDays: Provider.of<SettingsProvider>(context,
                                    listen: false)
                                .showDays),
                        textScaleFactor: 1.1,
                      ),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton.icon(
                          icon: Icon(Icons.access_time),
                          onPressed: () async {
                            await _selectTime(true);
                            setState2(() => selDate = selectedDate);
                          },
                          label: Text(
                            DateFormat.Hm(Localizations.localeOf(context)
                                    .languageCode)
                                .format(selDate),
                            textScaleFactor: 1.1,
                          ),
                        ),
                        Text('-'),
                        TextButton.icon(
                          icon: Icon(Icons.access_time),
                          onPressed: () async {
                            await _selectTime(false);
                            setState2(() => selDateEnd = selectedDateEnd);
                          },
                          label: Text(
                            DateFormat.Hm(Localizations.localeOf(context)
                                    .languageCode)
                                .format(selDateEnd),
                            textScaleFactor: 1.1,
                          ),
                        )
                      ],
                    )
            ],
          );
        }),
      ),
      actions: [
        TextButton(
          onPressed: () {
            note = '';
            Navigator.of(context).pop(false);
          },
          child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          child: Text(MaterialLocalizations.of(context).okButtonLabel),
        ),
      ],
    ),
  );
  if (ok != null && ok) {
    final tm = Provider.of<TimemanagerProvider>(context, listen: false);
    final end = settings.recordingType == RecordingType.Duration
        ? DateTime(
            selectedDate.year, selectedDate.month, selectedDate.day, 23, 59, 59)
        : selectedDateEnd;
    final start = settings.recordingType == RecordingType.Duration
        ? end.subtract(selectedDuration)
        : selectedDate;
    if (time == null || copy) {
      tm.create(
        Time(
          tm,
          note: note,
          task_uuid: task.uuid,
          start: start,
          end: end,
        ),
      );
    } else {
      tm.update(Time.copy(
        CopyBehaviour.updateCurrent,
        tm,
        time,
        note: note,
        start: start,
        end: end,
      ));
    }
    tm.sync();
  }
}
