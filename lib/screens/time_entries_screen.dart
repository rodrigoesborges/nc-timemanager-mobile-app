import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../helper/i18n_helper.dart';
import '../helper/time_update.dart';
import '../model/time.dart';
import '../provider/timemanager_provider.dart';
import '../widgets/app_drawer.dart';
import '../widgets/time_list_item.dart';

class TimeEntriesScreen extends StatefulWidget {
  static const routeName = '/times';

  @override
  _TimeEntriesScreenState createState() => _TimeEntriesScreenState();
}

class _TimeEntriesScreenState extends State<TimeEntriesScreen> {
  var _filterStartDate = DateTime(
    DateTime.now().year - 1,
    DateTime.now().month,
    DateTime.now().day,
  );
  var _filterEndDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
    23,
    59,
    59,
    999,
  );

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    var times = tm
        .getAll<Time>()
        .where((time) =>
            _filterStartDate.isBefore(time.end) &&
            _filterEndDate.isAfter(time.end))
        .toList(growable: false);
    times.sort((t1, t2) => t2.end.compareTo(t1.end));

    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text('general.time_entries'.tl(context)),
        actions: [
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: _setFilter,
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => tm.sync(),
        child: Scrollbar(
          child: ListView.builder(
            itemCount: times.length,
            itemBuilder: (context, i) => Column(
              children: [
                TimeListItem(
                  times[i],
                  updateTime,
                  showAllInfos: true,
                ),
                Divider(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _setFilter() async {
    Future<DateTime> _selectDate(DateTime selectedDate) async {
      final DateTime? picked = await showDatePicker(
          context: context,
          initialDate: selectedDate,
          firstDate: DateTime(2019, 1),
          lastDate: DateTime(DateTime.now().year + 1));
      return (picked != null && picked != selectedDate) ? picked : selectedDate;
    }

    var f2DateStart = _filterStartDate;
    var f2DateEnd = _filterEndDate;

    final ok = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        actionsPadding: EdgeInsets.all(5),
        buttonPadding: EdgeInsets.all(5),
        title: Text('general.filter'.tl(context)),
        content: SingleChildScrollView(
          child: StatefulBuilder(builder: (context, setState2) {
            return Column(
              children: [
                TextButton.icon(
                  icon: Icon(Icons.date_range),
                  onPressed: () async {
                    final newDate = await _selectDate(f2DateStart);
                    setState2(() => f2DateStart = newDate);
                  },
                  label: Text(
                    DateFormat.yMMMEd(
                            Localizations.localeOf(context).languageCode)
                        .format(f2DateStart),
                    textScaleFactor: 1.1,
                  ),
                ),
                const Text('-'),
                TextButton.icon(
                  icon: Icon(Icons.date_range),
                  onPressed: () async {
                    final newDate = await _selectDate(f2DateEnd);
                    setState2(() => f2DateEnd = newDate);
                  },
                  label: Text(
                    DateFormat.yMMMEd(
                            Localizations.localeOf(context).languageCode)
                        .format(f2DateEnd),
                    textScaleFactor: 1.1,
                  ),
                ),
              ],
            );
          }),
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(true);
            },
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
          ),
        ],
      ),
    );
    if (ok != null && ok) {
      setState(() {
        _filterStartDate = f2DateStart;
        _filterEndDate = DateTime(
            f2DateEnd.year, f2DateEnd.month, f2DateEnd.day, 23, 59, 59, 999);
      });
    }
  }
}
